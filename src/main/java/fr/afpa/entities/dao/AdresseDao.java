package fr.afpa.entities.dao;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/*
 * @autor Elvis
 */

@Data
@AllArgsConstructor 
@NoArgsConstructor 
@Entity 
public class AdresseDao {
	@Id 
	@GeneratedValue(strategy = GenerationType.SEQUENCE)  
	private Long idAdresse;
	@Column(name="numVoie",nullable = false) 
	private int numVoie;
	@Column(name="nomVoie",nullable = false) 
	private String nomVoie;
	@Column(name="codePostal",nullable = false)
	private int codePostal;
	@Column(name="ville",nullable = false)
	private String ville;
	@ManyToOne()
	
	@JoinColumn(name= "idUser")
	private UserDao user;
	
	
	public AdresseDao(int numVoie, String nomVoie, int codePostal, String ville) {
		super();
		this.numVoie = numVoie;
		this.nomVoie = nomVoie;
		this.codePostal = codePostal;
		this.ville = ville;
		
		
	}

	
}
